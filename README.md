# 视觉界面设计期末

### [项目前期竞品分析](https://gitee.com/nfunm067/visual_interface_design_midterm_project/blob/master/README.md)

### A3海报

[作品介绍海报](https://gitee.com/nfunm067/visual_interface_design_final/blob/master/%E4%BD%9C%E5%93%81%E6%B5%B7%E6%8A%A5.jpg)

[作品原型图海报](https://gitee.com/nfunm067/visual_interface_design_final/blob/master/%E4%BD%9C%E5%93%81%E5%9B%BE.jpg)

![输入图片说明](https://images.gitee.com/uploads/images/2020/0630/162948_065d8bd6_1648219.png "海报压缩.png")

### 品牌核心价值

![输入图片说明](https://images.gitee.com/uploads/images/2020/0629/222423_40a8e450_1648219.png "品牌核心价值.png")

### logo系统

![输入图片说明](https://images.gitee.com/uploads/images/2020/0630/004653_6e1a8ad5_1648219.png "logo系统.png")

### 颜色系统


![输入图片说明](https://images.gitee.com/uploads/images/2020/0630/011212_f1bbc8ad_1648219.png "颜色系统1.png")


![输入图片说明](https://images.gitee.com/uploads/images/2020/0629/201037_047f9f81_1648219.png "颜色系统.png")



### ICON视觉系统
![输入图片说明](https://images.gitee.com/uploads/images/2020/0708/234022_b9bfa51d_1532268.png "icon11.png")

### 字体系统
![输入图片说明](https://images.gitee.com/uploads/images/2020/0708/234044_6abbbc4d_1532268.jpeg "字体系统.jpg")

### 图片使用规范 
1. banner区域图片比例为2:1，圆角半径为20px
2. 瓷片区域图片比例为3:1，圆角半径为20px  
![输入图片说明](https://images.gitee.com/uploads/images/2020/0630/110050_873fb4d8_1648197.jpeg "banner.jpg")  
3. “我的”页面里头像尺寸规范为119*119，形状为圆形。
4. 其余页面的头像尺寸规范为99*99；形状为圆形。  
![输入图片说明](https://images.gitee.com/uploads/images/2020/0630/113327_f3c1695a_1648197.jpeg "头像大小.jpg")  
<br/></br>

5. 用户于“发现”页所分享的照片尺寸规范为221*221，形状为方形。每行内容最多只能容纳3张照片，最多3行。

### 版式、栏高、边距等尺寸规范+栅格系统说明 
#### 以IOS为例设计规范 
![输入图片说明](https://images.gitee.com/uploads/images/2020/0630/105521_51f7fcad_1648197.jpeg "规范.jpg")  

#### 栅格系统  
1. 屏幕总宽度：1334px
2. 水槽：20px
3. 列宽：100px
4. 列数：6列  
5. 安全边距：24px  
6. 内容总宽度：702px  
![输入图片说明](https://images.gitee.com/uploads/images/2020/0704/000952_727f656c_1648197.jpeg "栅格.jpg")  


#### 卡片规范  
1. 默认卡片比例为16:9，尺寸规范为702*395  
2. 课程-评价区-卡片：尺寸规范为702*234
3. 课程-调整课程卡片：尺寸规范为702*483
4. 课程-课程筛选卡片：尺寸规范为702*528
5. 卡片的圆角半径均为15px
6. 卡片阴影设置：  
![输入图片说明](https://images.gitee.com/uploads/images/2020/0630/105604_870cdf64_1648197.png "卡片阴影.png")  
<br/></br>

7. 卡片内容规范：上下边距为24px，左右边距为20px  
![输入图片说明](https://images.gitee.com/uploads/images/2020/0704/001002_f3ed5d6a_1648197.png "卡片上下边距.png")  

#### 按钮规范  
![输入图片说明](https://images.gitee.com/uploads/images/2020/0630/110130_4dbfd55a_1648197.jpeg "按钮.jpg")  

### [高保值原型图](http://nfunm067.gitee.io/test_preparation_prototype)


